/* class Settings */
class Settings{
  /* constructor */
  constructor(){
    // setup defaults
    this.setupDefaults();
  }


  /* setup defaults */
  setupDefaults(){
    // defaults
    this.ovr=false;                      // game over status
    this.secret=new Array();             // the color secret to find out
    this.sclen=4;                        // how long the secret should be
    this.steps=8;                        // how many steps max
    this.dbug=false;                     // debug mode
    this.nln=String.fromCharCode(10);    // line break
    this.dbl=false;                      // allow double colors
    this.btnaud='';                      // button audio
    this.clkaud='';                      // color button audio
    this.erraud='';                      // error audio
  }
}

/* class Tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* add class to element */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* do click on element by id */
  clickOnElementById(elmid){
    document.getElementById(elmid).click();
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* center game */
  centerWrapper(){
    var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
    document.getElementById('wrapper').style.marginLeft=left+'px';
    var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
    document.getElementById('wrapper').style.marginTop=top+'px';
  }

  /* get items for selector */
  getItemsForSelector(sel){
    return document.querySelectorAll(sel);
  }

  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

/* class Sound */
class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.btnaud=new Audio('button.wav');
    this.btnaud.volume=0.15;
    this.clkaud=new Audio('click.wav');
    this.clkaud.volume=0.5;
    this.erraud=new Audio('error.wav');
    this.erraud.volume=0.5;
  }
}

/* class Game */
class Game{
  /* constructor */
  constructor(){
    // objects
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    // event listener
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('resize',this);
  }


  /* generate html */
  generateGridHtml(){
    console.log('generate grid html');
    var html='';
    // rows for steps
    for(var r=0;r<this.SET.steps;r++){
      html+='<div id="row-'+r+'" class="row" attr-num="'+r+'">'+this.SET.nln;
      for(var c=0;c<this.SET.sclen;c++){html+='  <div class="box" attr-num="'+c+'"><div class="glare"></div></div>'+this.SET.nln;}
      for(var c=0;c<this.SET.sclen;c++){html+='  <div class="marker" attr-num="'+c+'"></div>'+this.SET.nln;}
      html+='  <div class="clear"></div>'+this.SET.nln;
      html+='</div>'+this.SET.nln;
    }
    // break
    html+='<div class="line"></div>'+this.SET.nln
    // color selectors
    html+='<div id="row-'+r+'" class="row clickable" attr-num="'+r+'">'+this.SET.nln;
    for(var c=0;c<this.SET.sclen;c++){html+='  <div class="box" id="sel-'+c+'" attr-col="-1"><div class="glare"></div></div>'+this.SET.nln;}
    html+='  <div class="clear"></div>'+this.SET.nln;
    html+='</div>'+this.SET.nln;
    // return
    return html;
  }

  /* set html to container */
  setGridHtml(html){
    console.log('set grid html');
    document.getElementById('grid').innerHTML=html;
  }

  /* set color after click */
  setColorForSelector(which){
    console.log('set selector color');
    // get items for possible colors
    var clrs=this.TLS.getItemsForSelector('.colors>.box');
    // one up (check to loop)
    var cnt=document.getElementById(which).getAttribute('attr-col');
    cnt++;
    if(cnt==clrs.length){cnt=0;}
    // get color number
    var col=clrs[cnt].getAttribute('attr-col');
    // clear and set selector color class
    document.getElementById(which).className='box';
    this.TLS.addClassToElementById(which,col);
    // set color number
    document.getElementById(which).setAttribute('attr-col',cnt);
  }

  /* clear color selectors */
  clearSelectorColors(){
    // get colorm selector items
    var sels=this.TLS.getItemsForSelector('.clickable>.box');
    // walk
    sels.forEach((item)=>{
      var itemid=item.getAttribute('id');
      // reset color classes and color numbers
      document.getElementById(itemid).className='box';
      document.getElementById(itemid).setAttribute('attr-col',-1);
    });
  }

  /* check if color selectors are empty */
  isColorSelectUnfinished(){
    // setup
    var amt=0;
    // get colorm selector items
    var sels=this.TLS.getItemsForSelector('.clickable>.box');
    // walk
    sels.forEach((item)=>{
      var itemid=item.getAttribute('id');
      var cls=this.TLS.getClassStringForElementById(itemid);
      // not empty if there is an additional color class
      if(cls!='box'){amt++;}
    });
    // amt<sclen means not all selectors were clicked
    return (amt<this.SET.sclen)?true:false;
  }


  /* create the color secret */
  createSecret(){
    console.log('create secret');
    // get items for possible colors
    var clrs=this.TLS.getItemsForSelector('.colors>.box');
    this.SET.secret=new Array();
    // create secret with sclen length (with double colors or not)
    while(this.SET.secret.length<this.SET.sclen){
      var num=Math.floor(Math.random()*clrs.length);
      if(this.SET.dbl==false&&this.SET.secret.includes(num)){continue;}
      this.SET.secret=this.TLS.addItemToArray(num,this.SET.secret);
    }
  }

  /* calculate answer to color choice */
  setAnswerMarker(){
    console.log('set answer marker');
    // only if four colors are selected
    if(this.isColorSelectUnfinished()==false){
      // move colors and marker
      for(var r=0;r<(this.SET.steps-1);r++){
        var boxes=document.getElementById('row-'+(r+1)).innerHTML;
        document.getElementById('row-'+r).innerHTML=boxes;
      }
      // last row number is loop end result
      var lrn=r;
      // get my color selection to match
      var sels=this.TLS.getItemsForSelector('.clickable>.box');
      var mycols=new Array();
      sels.forEach((item)=>{
        var itemid=item.getAttribute('id');
        var num=document.getElementById(itemid).getAttribute('attr-col');
        mycols=this.TLS.addItemToArray(num,mycols);
      });
      // move color selectors one up
      var lstrow=this.TLS.getItemsForSelector('#row-'+lrn+'>.box');
      for(var x=0;x<lstrow.length;x++){lstrow[x].className=sels[x].className;}
      // clear markers in last row
      var mrkitms=this.TLS.getItemsForSelector('#row-'+lrn+'>.marker');
      mrkitms.forEach((item)=>{item.className='marker';});
      // clear color selectors
      this.clearSelectorColors();
      // set hit markers
      var marker=new Array();
      var chked=new Array();
      // 1:check my colors in secret (every color only once)
      mycols.forEach((num)=>{
        if(this.SET.secret.includes(num)==true&&chked.includes(num)==false){
          marker=this.TLS.addItemToArray(1,marker);
          chked=this.TLS.addItemToArray(num,chked);
        }
      });
      // 2:check same position (my color same position as in secret)
      var pos=0;
      for(var x=0;x<mycols.length;x++){
        if(mycols[x]==this.SET.secret[x]){
          marker[pos]=2;
          pos++;
        }
      }
      // set
      for(var x=0;x<marker.length;x++){
        var cls='';
        if(marker[x]==1){cls=' white';}
        if(marker[x]==2){cls=' black';}
        mrkitms[x].className+=cls;
      }
    }
  }


  /* check if game has ended */
  checkForEndGame(){
    console.log('check end game');
    // top row full
    var items=this.TLS.getItemsForSelector('#row-0>.box');
    if(items[0].className!='box'){this.endGame();}
    // all markers black
    var lnr=this.SET.steps-1;
    var amt=0;
    var items=this.TLS.getItemsForSelector('#row-'+lnr+'>.marker');
    items.forEach((item)=>{
      if(item.className.includes('black')==true){amt++;}
    });
    if(amt==this.SET.sclen){this.endGame();}
  }

  /* end of game */
  endGame(){
    console.log('game ended');
    this.TLS.playSound(this.SND.erraud);
    // reveal secret on color selector
    var clrs=this.TLS.getItemsForSelector('.colors>.box');
    var sels=this.TLS.getItemsForSelector('.clickable>.box');
    for(var x=0;x<this.SET.secret.length;x++){
      var which=this.SET.secret[x];
      var col=clrs[which].getAttribute('attr-col');
      sels[x].className+=' '+col;
    }
    // game over
    this.SET.ovr=true;
  }


  /* init game */
  init(){
    console.log('init');
    console.log('debug: '+this.SET.dbug);
    console.log('doubles: '+this.SET.dbl);
    this.TLS.centerWrapper();
    this.setGridHtml(this.generateGridHtml());
    this.createSecret();
    this.SET.ovr=false;
    this.TLS.hideCover();
    setTimeout(()=>{this.TLS.addClassToElementById('wrapper','rotatein');},2500);
    console.log('ready!');
  }


  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
    }
  }

  /* left mouse click */
  leftClickEvent(event){
    // clicked on a button
    if(event.target.matches('.button')){
      this.TLS.playSound(this.SND.btnaud);
      var action=event.target.getAttribute('attr-action');
      console.log(action);
      // what to do?
      if(this.SET.ovr==true&&action=='new'){this.init();}
      if(this.SET.ovr==false&&action=='check'){this.setAnswerMarker();this.checkForEndGame();}
    }
    // clicked on a color selector
    if(this.SET.ovr==false&&event.target.matches('.clickable>.box')){
      this.TLS.playSound(this.SND.clkaud);
      var which=event.target.getAttribute('id');
      console.log('select color');
      this.setColorForSelector(which);
    }
  }

  /* key press event */
  keyEvent(event){
    switch(event.code){
      // n(ew)
      case 'KeyN':
        if(this.SET.ovr==true){this.TLS.clickOnElementById('new');}
        break;
      // c(heck)
      case 'KeyC':
        if(this.SET.ovr==false){this.TLS.clickOnElementById('check');}
        break;
    }
  }

  /* window resize event */
  resizeEvent(event){
    // stop and restart game
    this.endGame();
    this.init();
  }
}


/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
  }
},250);
